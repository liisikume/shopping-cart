from django.urls import path, include
from . import views
from .views import (ProductListView, ProductCreateView,
                    ProductDetailView, ProductUpdateView,
                    ProductDeleteView, CategoryListView, CategoryCreateView,
                    CategoryDetailView, CategoryUpdateView,
                    CategoryDeleteView)

urlpatterns = [
    path('', ProductListView.as_view(), name='home'),
    path('products/list/', ProductListView.as_view(), name='product_list'),
    path('products/create/', ProductCreateView.as_view(), name='product_create'),
    path('products/detail/<int:pk>/', ProductDetailView.as_view(), name='product_detail'),
    path('products/update/<int:pk>/', ProductUpdateView.as_view(), name='product_update'),
    path('products/delete/<int:pk>/', ProductDeleteView.as_view(), name='product_delete'),

    path('categories/', CategoryListView.as_view(), name='category_list'),
    path('categories/create/', CategoryCreateView.as_view(), name='category_create'),
    path('categories/detail/<int:pk>/', CategoryDetailView.as_view(), name='category_detail'),
    path('categories/update/<int:pk>/', CategoryUpdateView.as_view(), name='category_update'),
    path('categories/delete/<int:pk>/', CategoryDeleteView.as_view(), name='category_delete'),

    path('product/<int:pk>/add_to_cart/', views.add_to_cart, name='add_to_cart'),
    path('cart/', views.view_cart, name='view_cart'),
    path('add_to_cart/<int:product_id>/', views.add_to_cart, name='add_to_cart'),
    path('cart/item/<int:cart_item_id>/remove/', views.remove_from_cart, name='remove_from_cart'),
]
